package com.example.betabay.com.jernej.APIcalls;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.webkit.MimeTypeMap;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.betabay.Add;
import com.example.betabay.R;
import com.example.betabay.Settings;
import com.example.betabay.TriggerMe;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.greenrobot.eventbus.EventBus;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;


public class APIcalls {
    static Context context;
    static String tokenString;
    public static String url = "http://192.168.0.102:8001/";
    static RequestQueue requestQueue;

    public static void register(String iKnow,Context contextCall){
        context=contextCall;
        String userJson=toJson(Settings.whoami);
        Log.d("REGISTRACIJA klic",userJson);
        JSONObject postparams = null;
        try {
            postparams = new JSONObject(userJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url+"user/"+iKnow, postparams,
                response -> {
                    try {
                        Log.d("REGISTRACIJA response",response.toString());
                        Settings.whoami.tokenString=response.get("token").toString();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    EventBus.getDefault().post(new TriggerMe(1));

                },
                error -> {
                        Log.d("REGISTRACIJA error",error.toString());
                        })
        {
            @Override
            public Map getHeaders (){
            HashMap headers = new HashMap();
            headers.put("Content-Type", "application/json");
            return headers;
        }

        };
        addToRequestQueue(jsonObjReq, "postRequest");

    }


    public static void getAdds(Context contextCall){
        context=contextCall;
        String userJson=toJson(Settings.whoami);
        Log.d("GET ADDS klic",userJson);
        JsonArrayRequest jsonObjReq = new JsonArrayRequest(Request.Method.GET, url+"Adds/",null,
                response -> {
                    try{
                        Settings.allAdds.clear();
                        for(int i=0;i<response.length();i++) {
                            JSONObject AddJson = response.getJSONObject(i);
                            String BDid = AddJson.getString("DBid");
                            String title = AddJson.getString("title");
                            String content = AddJson.getString("content");
                            String longitude = AddJson.getString("longitude");
                            String latitude = AddJson.getString("latitude");
                            String userid = AddJson.getString("userid");
                            String created = AddJson.getString("created");
                            String price = AddJson.getString("price");
                            String photo = AddJson.getString("photo");
                            String phone = AddJson.getString("tel");
                            Add tmp = new Add(BDid, title, content, userid, created, price, photo, longitude, latitude,phone);
                            Settings.allAdds.add(tmp);
                            Log.d("GET ADDS", AddJson.toString());
                        }
                        EventBus.getDefault().post(new TriggerMe(4));
                    }catch (JSONException e){
                        e.printStackTrace();
                    }

                },
                error -> {
                    Log.d("GET ADDS","error response");
                }
        ) {
            @Override
            public Map getHeaders() {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization","Bearer "+Settings.whoami.tokenString);
                return headers;
            }
        };
        addToRequestQueue(jsonObjReq, "postRequest");

    }


    public static void createAdd(Context contextFrom,Add add){
        context=contextFrom;


        JSONObject postparams = null;
        try {
            postparams = new JSONObject();
            postparams.put("title",add.getTitle());
            postparams.put("content",add.getContentOfAdd());
            postparams.put("longitude",String.valueOf(add.getGeoPoint().getLongitude()));
            postparams.put("latitude",String.valueOf(add.getGeoPoint().getLatitude()));
            postparams.put("price",add.getPrice().toString());
            postparams.put("photo",add.getPhoto());
            postparams.put("tel",add.getPhone());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("SET ADD request",postparams.toString());
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url+"Adds/", postparams,
                response -> {
                    try {
                        Log.d("SET ADD response",response.get("status").toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    EventBus.getDefault().post(new TriggerMe(2));

                },
                error -> {
                    Log.d("GET ADDS error",error.toString());
                })
        {
            @Override
            public Map getHeaders (){
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization","Bearer "+Settings.whoami.tokenString);
                return headers;
            }

        };
        addToRequestQueue(jsonObjReq, "postRequest");

    }



    public static void uploadImage(Context contextFrom,File file){
        Log.d("IMAGE UPLOAD","image upload started");
        context=contextFrom;
        String contentType=getMimeTypeofFile(file.getPath());
        if(contentType=="cancer"){
            Log.d("IMAGE UPLOAD","you uploaded a cancer i will not upload");
            return;
        }
        Log.d("IMAGE UPLOAD","content type is "+contentType);
        String path=file.getAbsolutePath();
        Thread thread=new Thread(new Runnable() {
            @Override
            public void run() {
                OkHttpClient okHttpClient=new OkHttpClient();
                RequestBody fileBody=RequestBody.create(MediaType.parse(contentType),file);
                RequestBody requestBody=new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("type",contentType)
                        .addFormDataPart("myFile",path.substring(path.lastIndexOf("/")+1),fileBody)
                        .build();

                okhttp3.Request request=new okhttp3.Request.Builder()
                        .url(url+"Adds/uploadImage/"+Settings.whoami.uuid)
                        .post(requestBody)
                        .addHeader("Authorization","Bearer "+Settings.whoami.tokenString)
                        .build();

                try {
                    okhttp3.Response response=okHttpClient.newCall(request).execute();
                    if(!response.isSuccessful()){
                        throw new IOException("IMAGE UPLOAD ERROR"+response);
                    }else{
                        String responseData = response.body().string();
                        Log.e("IMAGE UPLOAD", "onResponse: " + responseData);
                        try {
                            JSONObject jsonObject = new JSONObject(responseData);
                            Settings.addPhotoString=jsonObject.get("status").toString().substring(jsonObject.get("status").toString().lastIndexOf("/")+1);
                            Log.e("IMAGE UPLOAD", "picture name is: " + Settings.addPhotoString);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                } catch (IOException e) {
                    Log.d("IMAGE UPLOAD","ERROR something went wrong");
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }




    private static String getMimeTypeofFile(String path){
        String fileExtension= MimeTypeMap.getFileExtensionFromUrl(path);
        Log.d("IMAGE UPLOAD","extension "+fileExtension);
        if(fileExtension.matches("jpg")){
            return MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension);
        }
        return "cancer";
    }

    public static <T> String toJson(T tmp) {
        Gson gson = new Gson();
        String rac_json=gson.toJson(tmp,tmp.getClass());
        return rac_json;
    }


    public static RequestQueue getRequestQueue() {
        if (requestQueue == null)
            requestQueue = Volley.newRequestQueue(context);

        return requestQueue;
    }


    public static void addToRequestQueue(Request request, String tag) {
        request.setTag(tag);
        getRequestQueue().add(request);

    }



}