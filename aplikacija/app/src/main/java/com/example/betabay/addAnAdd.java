package com.example.betabay;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.betabay.com.jernej.APIcalls.APIcalls;
import com.nbsp.materialfilepicker.MaterialFilePicker;
import com.nbsp.materialfilepicker.ui.FilePickerActivity;

import org.greenrobot.eventbus.EventBus;
import org.osmdroid.util.GeoPoint;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class addAnAdd extends AppCompatActivity implements GPSfragment.GPSFragmentListener {

    Button publish, cancel, upload;
    EditText title, content,price;
    GPSfragment gpsMainFragment;
    GeoPoint setGeopoint;
    String photoFile="noPhoto.jpg";
    ImageView imageView;
    Bitmap bitmap;
    public Add adableAdd;
    File f= null;
    int colorBack=0;
    int colorButton=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_an_add);
        Intent intent = getIntent();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        colorBack = intent.getIntExtra(Settings.EXTRA_COLOR, Color.WHITE);
        colorButton = intent.getIntExtra(Settings.EXTRA_COLOR_BUTTON,Color.GRAY);
        getWindow().getDecorView().setBackgroundColor(colorBack);


        initialize();
        if(colorBack!=0){
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        }

        gpsMainFragment=new GPSfragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.gpsContainer,gpsMainFragment).commit();

    }

    public void initialize() {
        publish = findViewById(R.id.buttonPublish);
        cancel = findViewById(R.id.buttonCancel);
        upload = findViewById(R.id.buttonUpload);
        title = findViewById(R.id.inputTitle);
        content = findViewById(R.id.inputContent);
        price=findViewById(R.id.editTextPrice);
        imageView=findViewById(R.id.imageViewOglasa);

        if (colorButton != 0) {
            publish.setBackgroundColor(colorButton);
            cancel.setBackgroundColor(colorButton);
            upload.setBackgroundColor(colorButton);
        }


        price.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(5,2)});

        publish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(title==null || price==null || content==null || Settings.whoami.location==null){
                    Toast.makeText(getApplicationContext(), "Error performing this request try again later",
                            Toast.LENGTH_LONG).show();
                }else{
                    if(f!=null){
                        APIcalls.uploadImage(getApplicationContext(),f);

                        if(Settings.addPhotoString==null){
                            Toast.makeText(getApplicationContext(), "Please wait we are uploading your picture",
                                    Toast.LENGTH_LONG).show();
                        }else{
                            adableAdd=new Add(title.getText().toString(),content.getText().toString(),Settings.whoami.location,Float.valueOf(price.getText().toString()),Settings.addPhotoString);
                            APIcalls.createAdd(getApplicationContext(),adableAdd);
                            EventBus.getDefault().post(new TriggerMe(2));
                            finish();
                        }
                    }else{
                        adableAdd=new Add(title.getText().toString(),content.getText().toString(),Settings.whoami.location,Float.valueOf(price.getText().toString()),"noPhoto.jpg");
                        APIcalls.createAdd(getApplicationContext(),adableAdd);
                        EventBus.getDefault().post(new TriggerMe(2));
                        finish();
                    }


                }

            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Take a picture of your product",
                        Toast.LENGTH_LONG).show();
                Intent intent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent,303);
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MaterialFilePicker()
                        .withActivity(addAnAdd.this)
                        .withRequestCode(10)
                        .start();
            }
        });
    }


    @Override
    public void onGPSFragmentSent(GeoPoint geoPoint) {
        setGeopoint=geoPoint;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode==10 && resultCode==RESULT_OK){
            try {
                f = new File(data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH));
            } catch (NullPointerException e) {
                Log.d("IMAGE UPLOAD","no file selected");
                e.printStackTrace();
                return;
            }
            try {
                bitmap= MediaStore.Images.Media.getBitmap(getContentResolver(),Uri.fromFile(f));
                imageView.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
                Log.d("IMAGE UPLOAD","file an image");
                return;
            }
        }
        if(requestCode==303 && resultCode==RESULT_OK){
            try {
                bitmap= (Bitmap)data.getExtras().get("data");
                imageView.setImageBitmap(bitmap);



                try {
                    String path = Environment.getExternalStorageDirectory().toString();
                    OutputStream fOut = null;
                    Integer counter = 0;
                    f = new File(path, Settings.whoami.uuid+".jpg");
                    fOut = new FileOutputStream(f);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
                    fOut.flush();
                    fOut.close();
                } catch (IOException e) {
                    Toast.makeText(getApplicationContext(), "ERROR saving file",
                            Toast.LENGTH_LONG).show();
                    Log.d("TAKE IMAGE","error saving");
                    e.printStackTrace();
                }


            } catch (NullPointerException e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), "imageDeleted",
                        Toast.LENGTH_LONG).show();
            }
        }
    }
}


class DecimalDigitsInputFilter implements InputFilter {

    Pattern mPattern;

    public DecimalDigitsInputFilter(int digitsBeforeZero,int digitsAfterZero) {
        mPattern=Pattern.compile("[0-9]{0," + (digitsBeforeZero-1) + "}+((\\.[0-9]{0," + (digitsAfterZero-1) + "})?)||(\\.)?");
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

        Matcher matcher=mPattern.matcher(dest);
        if(!matcher.matches())
            return "";
        return null;
    }

}