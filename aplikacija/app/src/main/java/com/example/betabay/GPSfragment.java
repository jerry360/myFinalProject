package com.example.betabay;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;


import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.ItemizedOverlayWithFocus;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.OverlayItem;

import java.io.File;
import java.util.ArrayList;

public class GPSfragment extends Fragment {
    Button myLocation;
    MapView map;
    Marker marker;
    GeoPoint geoPoint;
    public GPSFragmentListener listener;
    IMapController mapController;

    public interface GPSFragmentListener{
        void onGPSFragmentSent(GeoPoint geoPoint);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {



        View v= inflater.inflate(R.layout.gps_layout,container,false);

        map = (MapView)v.findViewById(R.id.map);
        map.setTileSource(TileSourceFactory.MAPNIK);
        map.setMultiTouchControls(true);

        myLocation=v.findViewById(R.id.thisLocationButton);

        mapController = map.getController();
        if(Settings.whoami.location!=null){
            mapController.animateTo(Settings.whoami.location,15.001,900L);
        }else{
            GeoPoint tmp_point=new GeoPoint(46.1512,14.9955);
            mapController.animateTo(tmp_point,8.001,900L);
        }

        Configuration.getInstance().setUserAgentValue(BuildConfig.APPLICATION_ID);


        myLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPositionMarker();
                geoPoint=new GeoPoint(1.11111,22.2222);
                listener.onGPSFragmentSent(geoPoint);
            }
        });

        return v;
    }

    private Marker getPositionMarker() { //Singelton
        if(Settings.whoami.location!=null){
                marker = new Marker(map);
                marker.setTitle("Here you are");
                marker.setPosition(Settings.whoami.location);
                marker.setIcon(null);
                mapController.animateTo(Settings.whoami.location,15.001,900L);
                map.getOverlays().add(marker);
        }else{
            Toast.makeText(getActivity(), "GETTING YOUR LOCATION!",
                    Toast.LENGTH_LONG).show();
        }
        return marker;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof GPSFragmentListener){
            listener=(GPSFragmentListener)context;
        }else{
            throw new RuntimeException(context.toString()+"NO LISTENER IMPLEMENTATION");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener=null;
    }

}
