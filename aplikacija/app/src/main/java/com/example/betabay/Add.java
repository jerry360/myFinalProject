package com.example.betabay;

import android.location.Location;

import com.android.volley.toolbox.StringRequest;

import org.osmdroid.util.GeoPoint;

import java.sql.Timestamp;

public class Add {
    int BDid;
    String Title;
    String Content;
    int UserID;
    Timestamp Created;
    Float Price;
    String Photo;
    GeoPoint geoPoint;
    String Phone;

    public Add(String BDid, String title, String content, String userID, String created, String price, String photo, String longitude, String latitude, String phone) {
        this.BDid = Integer.valueOf(BDid);
        Title = title;
        Content = content;
        UserID = Integer.valueOf(userID);
        Created = Timestamp.valueOf(created);
        Price = Float.valueOf(price);
        Photo = photo;
        geoPoint=new GeoPoint(Double.valueOf(latitude),Double.valueOf(longitude));
        Phone=phone;
    }

    public Add(String title, String content, GeoPoint location,Float price, String photo){
        Title=title;
        Content=content;
        geoPoint=location;
        Price=price;
        Photo=photo;
        Phone="031809866";
    }


    public int getBDid() {
        return BDid;
    }

    public String getTitle() {
        return Title;
    }

    public String getPhone() {
        return Phone;
    }

    public String getContentOfAdd() {
        return Content;
    }

    public int getUserID() {
        return UserID;
    }

    public Timestamp getCreated() {
        return Created;
    }

    public Float getPrice() {
        return Price;
    }

    public String getPhoto() {
        return Photo;
    }

    public GeoPoint getGeoPoint() {
        return geoPoint;
    }
}
