package com.example.betabay.com.jernej.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.betabay.Add;
import com.example.betabay.BuildConfig;
import com.example.betabay.R;
import com.example.betabay.Settings;
import com.example.betabay.com.jernej.APIcalls.APIcalls;
import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;
import com.wajahatkarim3.easyflipview.EasyFlipView;

import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class addAdapter extends PagerAdapter {


    private ArrayList<Add> models;
    private LayoutInflater layoutInflater;
    private Context context;
    private Marker marker;

    public addAdapter(ArrayList<Add> models, Context context) {
        this.models = models;
        this.context = context;
    }

    @Override
    public int getCount() {
        return models.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.add_layout, container, false);

        ImageView imageView;
        TextView title, desc,price;
        MapView map;

        map = view.findViewById(R.id.AddMap);
        map.setTileSource(TileSourceFactory.MAPNIK);
        map.setMultiTouchControls(true);
        IMapController mapController = map.getController();


        Configuration.getInstance().setUserAgentValue(BuildConfig.APPLICATION_ID);
        imageView = view.findViewById(R.id.image);
        title = view.findViewById(R.id.title);
        desc = view.findViewById(R.id.desc);
        price=view.findViewById(R.id.PriceTextView);
        EasyFlipView easyFlipView = (EasyFlipView) view.findViewById(R.id.FlipMe);


        if(models.get(position).getPhoto().matches("noPhoto.jpg")){
            Bitmap tmp_image = BitmapFactory.decodeResource(context.getResources(), R.drawable.no_image);
            imageView.setImageBitmap(tmp_image);
        }else{
            String url=APIcalls.url+"getImage/"+models.get(position).getPhoto();
            Picasso.get().load(url).into(imageView);
        }


        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        String tmpS=df.format(models.get(position).getPrice());
        tmpS+="€";
        price.setText(tmpS);
        title.setText(models.get(position).getTitle());
        desc.setText(models.get(position).getContentOfAdd());

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                easyFlipView.flipTheView();
                marker = new Marker(map);
                marker.setTitle("the seller is here");
                marker.setPosition(models.get(position).getGeoPoint());
                marker.setIcon(null);
                mapController.animateTo(models.get(position).getGeoPoint(),15.001,900L);
                map.getOverlays().add(marker);
            }
        });

        container.addView(view, 0);
        return view;
    }


    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View)object);
    }
}
