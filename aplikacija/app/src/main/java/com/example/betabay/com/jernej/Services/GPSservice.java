package com.example.betabay.com.jernej.Services;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.betabay.TrigerGPS;
import org.greenrobot.eventbus.EventBus;
import org.osmdroid.util.GeoPoint;

public class GPSservice extends Service {
    public LocationListener listener;
    public LocationManager meneger;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onCreate(){
        listener=new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Log.d("GPS SERVICE","location changed  "+location.getLongitude()+"   "+location.getLatitude());
                com.example.betabay.Settings.whoami.location=new GeoPoint(location.getLatitude(),location.getLongitude());
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {
                Intent i=new Intent((Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);

            }


        };

        meneger=(LocationManager)getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        meneger.requestLocationUpdates(LocationManager.GPS_PROVIDER,5000,2,listener);

    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if(meneger!=null){
            meneger.removeUpdates(listener);
        }
    }

}
