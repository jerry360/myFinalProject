package com.example.betabay;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.TextView;

public class UserSettings extends AppCompatActivity {
    int colorBack=0;
    int colorButtons=0;
    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_settings);
        Intent intent = getIntent();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        colorBack = intent.getIntExtra(Settings.EXTRA_COLOR, Color.WHITE);
        colorButtons = intent.getIntExtra(Settings.EXTRA_COLOR_BUTTON,Color.GRAY);
        getWindow().getDecorView().setBackgroundColor(colorBack);
    }
}
