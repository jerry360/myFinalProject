package com.example.betabay;


import android.content.Context;
import android.util.Log;
import com.example.betabay.com.jernej.APIcalls.APIcalls;
import com.example.betabay.com.jernej.fileReadWrite.Helper;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.osmdroid.util.GeoPoint;

import java.util.ArrayList;


public class Settings{
    public static User whoami;
    public static String filename;
    public Context context;
    public static ArrayList<Add> allAdds;
    public static String addPhotoString;
    public static final String EXTRA_COLOR = "setThis";
    public static final String EXTRA_COLOR_BUTTON = "setThisToButtons";

    public Settings(Context context) {
        whoami=new User(context);
        allAdds=new ArrayList<Add>();
        allAdds.add(new Add("starting the app","wait",new GeoPoint(Double.valueOf("1.0"),Double.valueOf("1.0")),Float.valueOf("0.0"),"noPhoto.jpg"));
        filename="userSettings";
        readSettings(context);
        EventBus.getDefault().register(this);
    }


    private void readSettings(Context context){
        this.context=context;
        String fileContent="";
        try {
            fileContent=Helper.preberi(context,filename);
        } catch (Exception e) {
            Log.d("///ERROR","APP STARTING FOR THE FIRST TIME!");
            e.printStackTrace();
        }
        if(fileContent!=""){
            JSONObject tmpJson=null;
            try {
                tmpJson=new JSONObject(fileContent);
                whoami.uuid=tmpJson.get("appid").toString();
                whoami.appid=tmpJson.get("uuid").toString();
                whoami.tokenString=tmpJson.get("tokenString").toString();
            } catch (JSONException e) {
                Log.d("///ERROR","JSON UNUSABLE!");
                e.printStackTrace();
                EventBus.getDefault().post(new TriggerMe(0));
            }
            Log.d("ALL GOOD",whoami.appid+"    "+whoami.uuid+"      "+whoami.tokenString);
            EventBus.getDefault().post(new TriggerMe(2));
        }else{
            writeSettings();
        }

    }

    private void writeSettings(){
        //to bos prek qr code dubu
        String whoIknow="pak";
        APIcalls.register(whoIknow,context);
    }

    private void writeRegistration(){
        JSONObject saveMe=new JSONObject();
        try {
            saveMe.put("appid",whoami.appid);
            saveMe.put("uuid",whoami.uuid);
            saveMe.put("tokenString",whoami.tokenString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String saveString=saveMe.toString();
        Log.d("REGISTRACIJA///SHRANIL BOM:",saveString);
        Helper.zapisi(saveString,filename,context);
        EventBus.getDefault().post(new TriggerMe(2));
    }

    @Subscribe
    public void onEvent(TriggerMe event) {
        if(event.Message==1){
            writeRegistration();
            Log.d("REGISTRACIJA","returning the wheel");
        }else if(event.Message==0){
            Log.d("REGISTRACIJA error","vrnil nulo");
        }
    }







    public User getWhoami() {
        return whoami;
    }

    public void setWhoami(User whoami) {
        this.whoami = whoami;
    }




}
