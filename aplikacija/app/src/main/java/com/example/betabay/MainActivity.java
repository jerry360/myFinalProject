package com.example.betabay;

import android.Manifest;
import android.animation.ArgbEvaluator;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import com.example.betabay.com.jernej.APIcalls.APIcalls;
import com.example.betabay.com.jernej.Adapters.addAdapter;
import com.example.betabay.com.jernej.Services.GPSservice;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class MainActivity extends AppCompatActivity {
    Settings settings;
    BroadcastReceiver broadcastReceiver;
    FloatingActionButton fab,fabAdd,fabSettings,fabAbout;
    ViewPager viewPager;
    addAdapter adapter;
    Button Buyme;
    int[] colors = null;
    int[] fabColors=null;
    int[] fabSettingsColors=null;
    int[] fabAddColors=null;
    int[] fabAboutColors=null;
    int colorPosition=0;
    ArgbEvaluator argbEvaluator = new ArgbEvaluator();
    Animation FabOpen,FabClose,FabClockWise,FabAntiClockWise;
    Boolean fabIsOpen=false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        EventBus.getDefault().register(this);
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);


        initialize();
        settings=new Settings(this.getApplicationContext());

        if(!permissions_ask()){
            enableButtonsGPS();
        }


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getApplicationContext(),addAnAdd.class);
                if(fabIsOpen){
                    fabAbout.startAnimation(FabClose);
                    fabSettings.startAnimation(FabClose);
                    fabAdd.startAnimation(FabClose);
                    fab.startAnimation(FabAntiClockWise);
                    fabAdd.setClickable(false);
                    fabSettings.setClickable(false);
                    fabAbout.setClickable(false);
                    fabIsOpen=false;
                }else{
                    fabAbout.startAnimation(FabOpen);
                    fabSettings.startAnimation(FabOpen);
                    fabAdd.startAnimation(FabOpen);
                    fab.startAnimation(FabClockWise);
                    fabAdd.setClickable(true);
                    fabSettings.setClickable(true);
                    fabAbout.setClickable(true);
                    fabIsOpen=true;
                }
            }
        });

        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getApplicationContext(),addAnAdd.class);
                if(colors!=null){
                    intent.putExtra(Settings.EXTRA_COLOR,colors[colorPosition]);
                    intent.putExtra(Settings.EXTRA_COLOR_BUTTON,fabColors[colorPosition]);
                }
                startActivity(intent);
            }
        });
        fabAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getApplicationContext(),AboutApp.class);
                if(colors!=null){
                    intent.putExtra(Settings.EXTRA_COLOR,colors[colorPosition]);
                    intent.putExtra(Settings.EXTRA_COLOR_BUTTON,fabColors[colorPosition]);
                }
                startActivity(intent);
            }
        });
        fabSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getApplicationContext(),UserSettings.class);
                if(colors!=null){
                    intent.putExtra(Settings.EXTRA_COLOR,colors[colorPosition]);
                    intent.putExtra(Settings.EXTRA_COLOR_BUTTON,fabColors[colorPosition]);
                }
                startActivity(intent);
            }
        });
        Buyme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                smsIntent.setType("vnd.android-dir/mms-sms");
                smsIntent.putExtra("address", Settings.allAdds.get(colorPosition).getPhone());
                smsIntent.putExtra("sms_body","i want to buy your product listed on BetaBay");
                startActivity(smsIntent);
            }
        });
    }

    public void enableButtonsGPS(){
        Intent i=new Intent(getApplicationContext(), GPSservice.class);
        startService(i);
    }

    public void initialize(){
        fabAdd = findViewById(R.id.fabAdd);
        Buyme=findViewById(R.id.buttonOrder);
        fab=findViewById(R.id.fab_open);
        fabSettings=findViewById(R.id.fab_setting);
        fabAbout=findViewById(R.id.fab_about);
        FabOpen= AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fab_open);
        FabClose= AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fab_close);
        FabClockWise= AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fab_rotate_clock_wise);
        FabAntiClockWise= AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fab_rotate_anti_clock_wise);
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == 100){
            if( grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED && grantResults[3] == PackageManager.PERMISSION_GRANTED){
                enableButtonsGPS();
            }else {
                permissions_ask();
            }
        }
    }


    @Override
    protected void onDestroy() {

        Intent i=new Intent(getApplicationContext(),GPSservice.class);
        stopService(i);

        super.onDestroy();
        if(broadcastReceiver!=null){
            unregisterReceiver(broadcastReceiver);
        }
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
//        Intent i=new Intent(getApplicationContext(),FacialRecognition.class);
//        startActivity(i);
    }

    public boolean permissions_ask(){
        if(Build.VERSION.SDK_INT>=23 & ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_COARSE_LOCATION)!=PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)!= PackageManager.PERMISSION_GRANTED){
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA,  Manifest.permission.READ_EXTERNAL_STORAGE},100);
            return true;
        }else {
            return false;
        }

    }



    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRegisterEvent(TriggerMe event) {
        if(event.Message==4){
            Log.d("GET ADDS","getting adds");
            loadedTheAdds();
        }else if(event.Message==2){
            Log.d("GET ADDS","pripravljen na klic");
            showAdds();
        }else if(event.Message==0){
            Log.d("REGISTRACIJA error","vrnil nulo");
        }
    }




    public void showAdds(){
        Log.d("GET ADDS","starting the call");
        APIcalls.getAdds(getApplicationContext());

    }


    public void loadedTheAdds(){
        Log.d("GET ADDS","call finished");
        adapter=new addAdapter(Settings.allAdds,getApplicationContext());
        viewPager=findViewById(R.id.viewPager);
        viewPager.setAdapter(adapter);
        viewPager.setPadding(90,0,90,0);

        ArrayList<Integer> colors_temp=new ArrayList<>();
        ArrayList<Integer> fabcolors_temp=new ArrayList<>();
        ArrayList<Integer> addfabcolors_temp=new ArrayList<>();
        ArrayList<Integer> settingsfabcolors_temp=new ArrayList<>();
        ArrayList<Integer> aboutfabcolors_temp=new ArrayList<>();

        for (Add add:Settings.allAdds) {
            Random rnd = new Random();
            int r=rnd.nextInt(150);
            int g=rnd.nextInt(150);
            int b=rnd.nextInt(150);
            int color = Color.argb(255, r,g,b);
            int fabcolor = Color.argb(255,r+50,g+50,b+50);
            int fabcolora = Color.argb(255,r+10,g+10,b+10);
            int fabcolors = Color.argb(255,r+25,g+25,b+25);
            int fabcolorab = Color.argb(255,r+40,g+40,b+40);
            colors_temp.add(color);
            fabcolors_temp.add(fabcolor);
            addfabcolors_temp.add(fabcolora);
            settingsfabcolors_temp.add(fabcolors);
            aboutfabcolors_temp.add(fabcolorab);
        }

        colors = colors_temp.stream().mapToInt(i->i).toArray();
        fabColors = fabcolors_temp.stream().mapToInt(i->i).toArray();
        fabSettingsColors = settingsfabcolors_temp.stream().mapToInt(i->i).toArray();
        fabAboutColors = addfabcolors_temp.stream().mapToInt(i->i).toArray();
        fabAddColors=aboutfabcolors_temp.stream().mapToInt(i->i).toArray();

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (position < (adapter.getCount() -1) && position < (colors.length - 1)) {
                    colorPosition=position;
                    viewPager.setBackgroundColor(

                            (Integer) argbEvaluator.evaluate(
                                    positionOffset,
                                    colors[position],
                                    colors[position + 1]
                            )
                    );

                    Buyme.setBackgroundColor(

                            (Integer) argbEvaluator.evaluate(
                                    positionOffset,
                                    fabColors[position],
                                    fabColors[position + 1]
                            )
                    );

                    fab.setBackgroundTintList(ColorStateList.valueOf(
                            (Integer) argbEvaluator.evaluate(
                            positionOffset,
                            fabColors[position],
                            fabColors[position + 1]
                    )));

                    fabAdd.setBackgroundTintList(ColorStateList.valueOf(
                            (Integer) argbEvaluator.evaluate(
                                    positionOffset,
                                    fabAddColors[position],
                                    fabAddColors[position + 1]
                            )));

                    fabSettings.setBackgroundTintList(ColorStateList.valueOf(
                            (Integer) argbEvaluator.evaluate(
                                    positionOffset,
                                    fabSettingsColors[position],
                                    fabSettingsColors[position + 1]
                            )));

                    fabAbout.setBackgroundTintList(ColorStateList.valueOf(
                            (Integer) argbEvaluator.evaluate(
                                    positionOffset,
                                    fabAboutColors[position],
                                    fabAboutColors[position + 1]
                            )));

                }

                else {
                    viewPager.setBackgroundColor(colors[colors.length - 1]);
                    fab.setBackgroundTintList(ColorStateList.valueOf(fabColors[fabColors.length - 1]));
                    fabAdd.setBackgroundTintList(ColorStateList.valueOf(fabAddColors[fabColors.length - 1]));
                    fabSettings.setBackgroundTintList(ColorStateList.valueOf(fabSettingsColors[fabColors.length - 1]));
                    fabAbout.setBackgroundTintList(ColorStateList.valueOf(fabAboutColors[fabColors.length - 1]));
                    Buyme.setBackgroundColor(fabColors[fabColors.length - 1]);
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

}
