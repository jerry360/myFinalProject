package com.example.betabay;

import android.content.Context;
import android.location.Location;
import android.provider.Settings;

import org.osmdroid.util.GeoPoint;

import java.util.UUID;

public class User {
    public String DBid;
    public String appid;
    public String uuid;
    public String tokenString;
    public GeoPoint location;

    public User(Context context) {
        this.appid = Settings.Secure.getString(context.getContentResolver(),Settings.Secure.ANDROID_ID);;
        this.uuid = UUID.randomUUID().toString();
        location=null;
    }
}
