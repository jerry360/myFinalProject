package main

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"bufio"
	"encoding/json"
	"net/http"
	"os"

	_ "github.com/go-sql-driver/mysql"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"

	jwtmiddleware "github.com/auth0/go-jwt-middleware"
	"github.com/dgrijalva/jwt-go"
)

var mySigningKey = []byte("PASSWORD")

type User struct {
	DBid        string
	UUID        string `json:"uuid"`
	AppID       string `json:"appid"`
	TokenString string `json:"token"`
}

type Add struct {
	DBid      string
	Title     string  `json:"title"`
	Content   string  `json:"content"`
	Longitude string  `json:"longitude"`
	Latitude  string  `json:"latitude"`
	UserID    int     `json:"userid"`
	Created   string  `json:"created"`
	Price     string  `json:"price"`
	Photo     string  `json:"photo"`
	PhoneNumber     string  `json:"tel"`
}

type State struct {
	Status string `json:"status"`
}

type Image struct {
	imageFile []byte `json:"imageFile"`
}

var jwtMiddleware = jwtmiddleware.New(jwtmiddleware.Options{
	ValidationKeyGetter: func(token *jwt.Token) (interface{}, error) {
		return mySigningKey, nil
	},
	Extractor:     jwtmiddleware.FromFirst(jwtmiddleware.FromAuthHeader),
	UserProperty:  "user",
	SigningMethod: jwt.SigningMethodHS256,
})

func getAdds(writer http.ResponseWriter, r *http.Request) {

	user := r.Context().Value("user")
	fmt.Fprintf(os.Stdout, "This is an authenticated request")
	fmt.Fprintf(os.Stdout, "Claim content:\n")
	for k, v := range user.(*jwt.Token).Claims.(jwt.MapClaims) {
		fmt.Fprintf(os.Stdout, "%s :\t%#v\n", k, v)
	}

	db, err := sql.Open("mysql", "me:password@tcp(127.0.0.1:3306)/Alphabay")
	if err != nil {
		log.Fatal(err)
		panic(err.Error())
	}

	err = db.Ping()
	if err != nil {
		panic(err.Error())
	} else {
		fmt.Println("CONNECTED TO DATABASE!")
	}

	var AllAdds []Add

	resault, err := db.Query("select * from Adds")
	if err != nil {
		panic(err.Error())
	} else {
		for resault.Next() {
			var tmpadd Add
			err = resault.Scan(&tmpadd.DBid, &tmpadd.Title, &tmpadd.Content, &tmpadd.Created, &tmpadd.Photo, &tmpadd.UserID, &tmpadd.Longitude, &tmpadd.Latitude, &tmpadd.Price,&tmpadd.PhoneNumber)
			if err != nil {
				panic(err.Error())
			} else {
				fmt.Println(tmpadd.DBid, tmpadd.Title, tmpadd.Content, tmpadd.Created, tmpadd.Price, tmpadd.Photo, tmpadd.UserID, tmpadd.Longitude, tmpadd.Latitude,tmpadd.PhoneNumber)
				AllAdds = append(AllAdds, tmpadd)
			}
		}
	}

	writer.Header().Set("Content-Type", "application/json")
	json.NewEncoder(writer).Encode(AllAdds)
	defer db.Close()
}

func getToken(writer http.ResponseWriter, r *http.Request) {
	writer.Header().Set("Content-Type", "application/json")
	var user User
	_ = json.NewDecoder(r.Body).Decode(&user)

	db, err := sql.Open("mysql", "me:password@tcp(127.0.0.1:3306)/Alphabay")
	if err != nil {
		log.Fatal(err)
		panic(err.Error())
	}

	err = db.Ping()
	if err != nil {
		panic(err.Error())
	} else {
		fmt.Println("CONNECTED TO DATABASE!")
	}

	preperd, err := db.Prepare("select * from User where UUID=? and AppID=?")
	resault, err := preperd.Query(user.UUID, user.AppID)
	if err != nil {
		panic(err.Error())
	} else {
		for resault.Next() {
			var tmpuser User
			err = resault.Scan(&tmpuser.DBid, &tmpuser.AppID, &tmpuser.UUID)
			if err != nil {
				panic(err.Error())
			} else {
				fmt.Println(tmpuser.AppID + " in " + tmpuser.UUID)
				token := jwt.New(jwt.SigningMethodHS256)
				claims := token.Claims.(jwt.MapClaims)
				claims["uuid"] = tmpuser.UUID
				claims["appid"] = tmpuser.AppID
				claims["id"] = tmpuser.DBid
				tokenString, _ := token.SignedString(mySigningKey)
				writer.Write([]byte(tokenString))
				defer db.Close()
				return
			}
		}
	}
	writer.WriteHeader(http.StatusUnauthorized)
}

func createAdd(writer http.ResponseWriter, r *http.Request) {
	var USERID string

	user := r.Context().Value("user")
	fmt.Fprintf(os.Stdout, "This is an authenticated request")
	fmt.Fprintf(os.Stdout, "Claim content:\n")
	for k, v := range user.(*jwt.Token).Claims.(jwt.MapClaims) {
		fmt.Fprintf(os.Stdout, "%s :\t%#v\n", k, v)
		if k == "id" {
			USERID = v.(string)
		}
	}
	SENDER, err := strconv.Atoi(USERID)
	fmt.Println("USERID is:", SENDER)
	writer.Header().Set("Content-Type", "application/json")
	var tmpAdd Add
	_ = json.NewDecoder(r.Body).Decode(&tmpAdd)
	fmt.Println("/////inserting add: ", tmpAdd.DBid, tmpAdd.Title, tmpAdd.Content, tmpAdd.Created, tmpAdd.Price, tmpAdd.Photo, SENDER, tmpAdd.Longitude, tmpAdd.Latitude,tmpAdd.PhoneNumber)

	db, err := sql.Open("mysql", "me:password@tcp(127.0.0.1:3306)/Alphabay")
	if err != nil {
		log.Fatal(err)
		panic(err.Error())
	}

	err = db.Ping()
	if err != nil {
		panic(err.Error())
	} else {
		fmt.Println("CONNECTED TO DATABASE!")
	}

	preperd, err := db.Prepare("insert into Adds(Title,Content,Created,Price,Photo,UserID,Longitude,Latitude,Phone) values(?,?,now(),?,?,?,?,?,?)")
	resault, err := preperd.Query(tmpAdd.Title, tmpAdd.Content, tmpAdd.Price, tmpAdd.Photo, SENDER, tmpAdd.Longitude, tmpAdd.Latitude,tmpAdd.PhoneNumber)
	if err != nil {
		fmt.Println("///////////////////////////NAPAKA PRI VPISOVANJU")
		panic(err.Error())
	} else {
		var st = 0
		for resault.Next() {
			st++
		}
		if st > 1 {
			fmt.Println("napaka rows:", st)
			writer.Header().Set("Content-Type", "application/json")
			var tmpStatus State
			tmpStatus.Status = "error"
			json.NewEncoder(writer).Encode(tmpStatus)
			fmt.Print(st)
		} else {
			writer.Header().Set("Content-Type", "application/json")
			var tmpStatus State
			tmpStatus.Status = "all good"
			json.NewEncoder(writer).Encode(tmpStatus)
		}

	}
	defer db.Close()
}

func searchAdds(writer http.ResponseWriter, r *http.Request) {
	fmt.Println("/////SEARCHING//////")
	var USERID string

	user := r.Context().Value("user")
	fmt.Fprintf(os.Stdout, "This is an authenticated request")
	fmt.Fprintf(os.Stdout, "Claim content:\n")
	for k, v := range user.(*jwt.Token).Claims.(jwt.MapClaims) {
		fmt.Fprintf(os.Stdout, "%s :\t%#v\n", k, v)
		if k == "id" {
			USERID = v.(string)
		}
	}
	SENDER, err := strconv.Atoi(USERID)
	fmt.Println("USERID is:", SENDER)

	writer.Header().Set("Content-Type", "application/json")
	var tmpAdd Add
	_ = json.NewDecoder(r.Body).Decode(&tmpAdd)
	fmt.Println("/////searching add: ", tmpAdd.DBid, tmpAdd.Title, tmpAdd.Content, tmpAdd.Created, tmpAdd.Price, tmpAdd.Photo, SENDER, tmpAdd.Longitude, tmpAdd.Latitude,tmpAdd.PhoneNumber)
	if tmpAdd.UserID == -1 {
		tmpAdd.UserID = SENDER
	}

	db, err := sql.Open("mysql", "me:password@tcp(127.0.0.1:3306)/Alphabay")
	if err != nil {
		log.Fatal(err)
		panic(err.Error())
	}

	err = db.Ping()
	if err != nil {
		panic(err.Error())
	} else {
		fmt.Println("CONNECTED TO DATABASE!")
	}

	var AllAdds []Add

	preperd, err := db.Prepare("select * from Adds where Title=? or Content=? or UserID=?")
	resault, err := preperd.Query(tmpAdd.Title, tmpAdd.Content, tmpAdd.UserID)
	if err != nil {
		panic(err.Error())
	} else {
		for resault.Next() {
			var tmpadd Add
			err = resault.Scan(&tmpadd.DBid, &tmpadd.Title, &tmpadd.Content, &tmpadd.Created, &tmpadd.Photo, &tmpadd.UserID, &tmpadd.Longitude, &tmpadd.Latitude, &tmpadd.Price, &tmpadd.PhoneNumber)
			if err != nil {
				panic(err.Error())
			} else {
				fmt.Println("/////adds found: ", tmpAdd.DBid, tmpAdd.Title, tmpAdd.Content, tmpAdd.Created, tmpAdd.Price, tmpAdd.Photo, SENDER, tmpAdd.Longitude, tmpAdd.Latitude,tmpAdd.PhoneNumber)
				AllAdds = append(AllAdds, tmpadd)
			}
		}
	}

	writer.Header().Set("Content-Type", "application/json")
	json.NewEncoder(writer).Encode(AllAdds)
	defer db.Close()
}

func addUser(writer http.ResponseWriter, r *http.Request) {
	parameter := mux.Vars(r)
	fmt.Println("///LOOKING FOR USER:", parameter["id"])
	writer.Header().Set("Content-Type", "application/json")
	var user User
	_ = json.NewDecoder(r.Body).Decode(&user)

	db, err := sql.Open("mysql", "me:password@tcp(127.0.0.1:3306)/Alphabay")
	if err != nil {
		log.Fatal(err)
		panic(err.Error())
	}

	err = db.Ping()
	if err != nil {
		panic(err.Error())
	} else {
		fmt.Println("CONNECTED TO DATABASE!")
	}

	preperd, err := db.Prepare("select * from User where UUID=?")
	resault, err := preperd.Query(parameter["id"])
	if err != nil {
		panic(err.Error())
	} else {
		for resault.Next() {
			var tmpuser User
			err = resault.Scan(&tmpuser.DBid, &tmpuser.AppID, &tmpuser.UUID)
			if err != nil {
				panic(err.Error())
			} else {
				fmt.Println("ADDING USER PERMITED BY appid:", tmpuser.AppID, "  uuid:", tmpuser.UUID, "   db:", tmpuser.DBid)
				preperd, err := db.Prepare("insert into User (AppID,UUID) values(?,?)")
				resault, err := preperd.Query(user.AppID, user.UUID)
				if err != nil {
					panic(err.Error())
				} else {
					var st = 0
					for resault.Next() {
						st++
					}
					if st > 1 {
						fmt.Println("napaka rows:", st)
						fmt.Print(st)
					} else {
						preperd, err := db.Prepare("select idUser from User where UUID=?")
						resault, err := preperd.Query(user.UUID)
						if err != nil {
							panic(err.Error())
						} else {
							for resault.Next() {
								err = resault.Scan(&user.DBid)
								if err != nil {
									panic(err.Error())
								} else {
									fmt.Println("GENERATING TOKEN FOR " + user.AppID + " in " + user.UUID + " z idbaze " + user.DBid)
									token := jwt.New(jwt.SigningMethodHS256)
									claims := token.Claims.(jwt.MapClaims)
									claims["uuid"] = user.UUID
									claims["appid"] = user.AppID
									claims["id"] = user.DBid
									tokenString, _ := token.SignedString(mySigningKey)
									user.TokenString = tokenString
									writer.Header().Set("Content-Type", "application/json")
									json.NewEncoder(writer).Encode(user)
								}
							}
						}
					}

				}

			}
		}

		writer.WriteHeader(http.StatusUnauthorized)
		defer db.Close()
	}
}

func setImage(writer http.ResponseWriter, r *http.Request) {
	var tmpStatus State
	tmpStatus.Status = "error"
	parameter := mux.Vars(r)
	fmt.Println("///IMAGE NAME:", parameter["id"])
	writer.Header().Set("Content-Type", "application/json")
	r.ParseMultipartForm(10 << 20)
	file, handler, err := r.FormFile("myFile")
	if err != nil {
		json.NewEncoder(writer).Encode(tmpStatus)
		fmt.Println(err)
		return
	}
	defer file.Close()
	fmt.Printf("Uploaded File: %+v\n", handler.Filename)
	fmt.Printf("File Size: %+v\n", handler.Size)
	fmt.Printf("MIME Header: %+v\n", handler.Header)

	tempFile, err := ioutil.TempFile("uploads", parameter["id"]+"___"+handler.Filename+"___")
	if err != nil {
		fmt.Println(err)
		json.NewEncoder(writer).Encode(tmpStatus)
		return
	}
	defer tempFile.Close()

	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println(err)
		json.NewEncoder(writer).Encode(tmpStatus)
		return
	}
	tempFile.Write(fileBytes)
	fmt.Println("CREATED FILE:" + tempFile.Name())
	tmpStatus.Status = tempFile.Name()
	json.NewEncoder(writer).Encode(tmpStatus)

}

func getImage(writer http.ResponseWriter, r *http.Request) {
	var tmpStatus State
	tmpStatus.Status = "error"
	parameter := mux.Vars(r)
	fmt.Println("///GET IMAGE WITH NAME:", parameter["id"])
	writer.Header().Set("Content-Type", "application/json")

	f, _ := os.Open("uploads/"+parameter["id"])

	// Read the entire JPG file into memory.
	reader := bufio.NewReader(f)
	content, _ := ioutil.ReadAll(reader)

	// Set the Content Type header.
	writer.Header().Set("Content-Type", "image/jpeg")

	// Write the image to the response.
	writer.Write(content)



}

func main() {
	fmt.Println("[+]listening on port :8001")
	router := mux.NewRouter().StrictSlash(true)
	router.Use(func(h http.Handler) http.Handler { return handlers.LoggingHandler(os.Stdout, h) })
	router.Use(handlers.CORS())

	router.HandleFunc("/getImage/{id}", getImage)
	router.HandleFunc("/getImage/{id}", getImage)
	router.HandleFunc("/user/{id}", addUser).Methods("POST")
	router.HandleFunc("/auth/getToken", getToken).Methods("POST")
	AddsRouter := router.PathPrefix("/Adds").Subrouter()
	AddsRouter.Use(jwtMiddleware.Handler)
	AddsRouter.HandleFunc("/", getAdds).Methods("GET")
	AddsRouter.HandleFunc("/uploadImage/{id}", setImage).Methods("POST")
	AddsRouter.HandleFunc("/", createAdd).Methods("POST")
	AddsRouter.HandleFunc("/search", searchAdds).Methods("GET")
	log.Fatal(http.ListenAndServe(":8001", router))
}
